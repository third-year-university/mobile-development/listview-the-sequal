package ru.dolbak.listview

enum class Sex {
    MAN, WOMAN, UNKNOWN
}